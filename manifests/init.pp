class ldapclient (
    String $conffile,
    String $package_name,
    Array[String] $ldap_uri,
    String $ldap_base,
    String $ldap_tlsreqcert,
    String $ldap_tlscacert,
    Enum['file', 'dir'] $ldap_tlscacerttype,
    Boolean $use_nssldap,
    Optional[String] $default_shell = undef,
    Optional[String] $override_shell = undef,
    Hash[String,String] $ldap_nss_overrides,
) {
    file { 'ldap.conf':
        path => "$conffile",
        ensure => file,
	owner => 'root',
	group => 'root',
        mode => '0644',
        require => Package['ldap_clients'],
        content => epp('ldapclient/etc/ldap/ldap.conf.epp', {
	    'ldap_uri' => $ldap_uri,
	    'ldap_base' => $ldap_base,
	    'ldap_tlsreqcert' => $ldap_tlsreqcert,
	    'ldap_tlscacert' => $ldap_tlscacert,
	    'ldap_tlscacerttype' => $ldap_tlscacerttype,
	}),
    }

    package { 'ldap_clients':
        name => $package_name,
        ensure => 'installed',
    }

    if $use_nssldap {
	file { 'nss_ldap.conf':
	    path => '/etc/ldap.conf',
	    ensure => file,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('ldapclient/etc/ldap.conf.epp', {
		'ldap_uri' => $ldap_uri,
		'ldap_base' => $ldap_base,
		'ldap_tlsreqcert' => $ldap_tlsreqcert,
		'ldap_tlscacert' => $ldap_tlscacert,
		'ldap_tlscacerttype' => $ldap_tlscacerttype,
	        'ldap_nss_overrides' => $ldap_nss_overrides,
	    }),
	}
    }
}
